var Robot = function(name) {
    this.name = name;
    this.isFunctional = true;
}

Robot.prototype.sayHi = function(toWho) {
  if (this.isFunctional === true) {
    console.log('Robot ' + this.name + ' greets ' + 'toWho');
  } else {
    console.log('Robot ' + this.name + ' is broken');
  }
};

var newname = 'Robot3';

Robot.prototype.changeName = function(newname) {
    console.log('Robot ' + this.name + ' changes name to ' + newname);
};

Robot.prototype.fixIt = function() {
    this.isFunctional = true;

    console.log('Robot ' + this.name + ' was fixed');
};
